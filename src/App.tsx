import { useEffect, useState } from 'react';
import { Alert } from 'antd';
import Select from './search/Select';
import type { OnChange } from './search/Select';
import ListContributors from './contributors/List';
import getProjectContributors, { noneResponse } from './contributors/get-list';
import './App.sass';

function App() {
  const abortController = new AbortController();
  const [selectedProjectSlug, setSelectedProjectSlug] = useState<string>('');
  const [projectContributors, setProjectContributors] = useState(
    noneResponse(),
  );

  const handleSelectChange: OnChange = (newValue, actionMeta) => {
    if (actionMeta.action !== 'select-option') {
      return;
    }

    setSelectedProjectSlug(() => newValue!.value);
  };

  useEffect(() => {
    if (selectedProjectSlug === '') {
      return;
    }

    getProjectContributors(abortController.signal, selectedProjectSlug).then(
      (response) => setProjectContributors(response),
    );

    return () => {};
  }, [selectedProjectSlug]);

  return (
    <div className='app'>
      <Select
        abortController={abortController}
        onChange={handleSelectChange}
      />

      {projectContributors.present(
        (contributors) => (
          <ListContributors contributors={contributors} />
        ),
        (error) => (
          <Alert
            message='Error Loading Project Contributors List'
            description={error}
            closable
            type='error'
            showIcon
          />
        ),
      )}
    </div>
  );
}

export default App;
