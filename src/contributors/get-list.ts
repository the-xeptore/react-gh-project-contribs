import axios from 'axios';
import { SearchTextUpdated } from '../search/constants';
import type { Presentable, ProjectContributor } from './types';

export default async function getProjectContributors(
  signal: AbortSignal,
  projectSlug: string,
): Promise<Presentable> {
  interface RawProjectContributor {
    readonly html_url: string;
    readonly contributions: number;
    readonly avatar_url: string;
    readonly login: string;
  }

  return await axios
    .get(`https://api.github.com/repos/${projectSlug}/contributors`, {
      params: {
        per_page: 10,
        page: 1,
      },
      signal,
      validateStatus: null,
    })
    .then((response) => {
      if (response.status !== 200) {
        if (response.status === 403) {
          return errorResponse(response.data.message);
        }

        return errorResponse('Unknown error occurred');
      }

      return okResponse(
        response.data.map((contributor: RawProjectContributor) => {
          const projectContributor: ProjectContributor = {
            htmlUrl: contributor.html_url,
            contributions: contributor.contributions,
            avatarUrl: contributor.avatar_url,
            login: contributor.login,
          };

          return projectContributor;
        }),
      );
    })
    .catch((error) => {
      if (signal.aborted) {
        if (signal.reason === SearchTextUpdated) {
          return errorResponse('Request cancelled due to search term change');
        }

        return errorResponse('Request cancelled');
      }

      return errorResponse(`${error}`);
    });
}

function okResponse(
  projectContributors: readonly ProjectContributor[],
): Presentable {
  return {
    present: (onOk, _) => onOk(projectContributors),
  };
}

function errorResponse(error: string): Presentable {
  return {
    present: (_, onError) => onError(error),
  };
}

export function noneResponse(): Presentable {
  return {
    present: () => null as unknown as JSX.Element,
  };
}
