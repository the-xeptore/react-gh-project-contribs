export interface ProjectContributor {
  readonly htmlUrl: string;
  readonly contributions: number;
  readonly avatarUrl: string;
  readonly login: string;
}

export interface Presentable {
  readonly present: (
    onOk: (ok: readonly ProjectContributor[]) => JSX.Element,
    onError: (error: string) => JSX.Element,
  ) => JSX.Element;
}
