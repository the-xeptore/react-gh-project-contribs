import { Card } from 'antd';
import { ProjectContributor } from './types';

export interface ListContributorsProps {
  readonly contributors: readonly ProjectContributor[];
}

export default function ListContributors(props: ListContributorsProps) {
  return (
    <section className='contributors-card'>
      <section className='space'>
        {props.contributors.map((projectContributor) => (
          <Card
            className='card'
            key={projectContributor.login}
            hoverable
            style={{ width: 240 }}
            cover={
              <a href={projectContributor.htmlUrl}>
                <img
                  alt={projectContributor.login}
                  className='contributor-avatar'
                  src={projectContributor.avatarUrl}
                />
              </a>
            }
          >
            <Card.Meta
              title={
                <a href={projectContributor.htmlUrl}>
                  {projectContributor.login}
                </a>
              }
              description={
                <p>
                  <code>Contributions: {projectContributor.contributions}</code>
                </p>
              }
            />
          </Card>
        ))}
      </section>
    </section>
  );
}
