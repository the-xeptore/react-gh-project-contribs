import type { ActionMeta } from 'react-select';
import AsyncSelect from 'react-select/async';
import debounce from 'lodash.debounce';
import type { Option } from './fetch-options';
import fetchOptions from './fetch-options';
import { SearchTextUpdated } from './constants';

export type OnChange = (
  newValue: Option | null,
  actionMeta: ActionMeta<Option>,
) => void;

export interface SelectProps {
  readonly onChange: OnChange;
  readonly abortController: AbortController;
}

export default function Select(props: SelectProps) {
  let abortController: AbortController | null = null;

  const debouncedLoadOptions = debounce(
    (inputValue: string, callback: (options: readonly Option[]) => void) => {
      abortController = new AbortController();
      const { signal } = abortController;
      signal.addEventListener(
        'abort',
        () => props.abortController.abort(signal.reason),
        { once: true },
      );
      fetchOptions(signal, inputValue, callback);
    },
    350,
  );

  function loadOptions(
    inputValue: string,
    callback: (options: readonly Option[]) => void,
  ): void {
    abortController?.abort(SearchTextUpdated);
    debouncedLoadOptions(inputValue, callback);
  }

  return (
    <AsyncSelect
      className='select'
      placeholder='Search for GitHub Project Name...'
      onChange={props.onChange}
      isClearable
      autoFocus
      escapeClearsValue
      isSearchable
      cacheOptions
      loadOptions={loadOptions}
    />
  );
}
