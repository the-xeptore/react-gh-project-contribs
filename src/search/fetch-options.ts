import axios from 'axios';
import { SearchTextUpdated } from './constants';

export interface Option {
  readonly label: string;
  readonly value: string;
}

export default function fetchOptions(
  abortSignal: AbortSignal,
  inputValue: string,
  callback: (options: readonly Option[]) => void,
): void {
  axios
    .get('https://api.github.com/search/repositories', {
      params: {
        q: encodeURIComponent(inputValue),
        per_page: 15,
        page: 1,
      },
      signal: abortSignal,
    })
    .then((response) => {
      if (response.status !== 200) {
        return callback([]);
      }

      const output: readonly Option[] = response.data.items.map(
        (item: { readonly full_name: string }) => {
          const option: Option = {
            label: item.full_name,
            value: item.full_name,
          };

          return option;
        },
      );

      return callback(output);
    })
    .catch((error) => {
      if (abortSignal.aborted) {
        if (abortSignal.reason === SearchTextUpdated) {
          return callback([]);
        }
      }

      // FIXME: Unknown error case. Should be propagated, and shown to the user.
      // But for now, just assume there were no results matched the search term.
      // throw error;
      return callback([]);
    });
}
