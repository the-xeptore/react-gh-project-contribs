# GitHub Project Contributors

Flexiana Coding Challenge Project in React

Visit live version at: <https://the-xeptore.gitlab.io/react-gh-project-contribs>

## Develop

1. Clone the project
2. Install [pnpm](https://pnpm.io/)
3. Install dependencies

    ```sh
    pnpm install
    ```

4. Run the project

    ```sh
    pnpm run dev
    ```

5. Access it via: <http://localhost:5173/>

## TODO

- [x] Setup GitLab Pages
